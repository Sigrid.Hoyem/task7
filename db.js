const { Pool } = require('pg');

class Db {
    constructor() {
        this.config = {
            user: 'biidtzpcgxnsjj',
            host: 'ec2-54-83-9-36.compute-1.amazonaws.com',
            database: 'd325snv4kvded4',
            password: '89d3fdff165ccf04e72d9851c3677bce2d298368faebd4d0de7b73a223a73daa',
            port: 5432,
        };
        this.pool = new Pool(this.config);
    }

    getPool() {
        return this.pool;
    }

    getPlanets(request, response) {
        this.pool.query('SELECT * FROM planet', (error, results) => {
            if (error) {
                response.status(500).json({
                    success: false,
                    message: 'error',
                });
            }
            response.json(results.rows);
        });
    }

    async addPlanet(request, response) {
        const { name, size, date_discovered } = request.body;
        try {
            const planetsWithName = await this.pool.query('SELECT * FROM planet WHERE name= $1', [name]);
            if (planetsWithName.rowCount > 0) {
                response.status(400).json({
                    success: false,
                    message: 'Planet already in database',
                });
            }
            else {
                this.pool.query('INSERT INTO planet(name, size, date_discovered) VALUES ($1, $2, $3) RETURNING id, name, size, date_discovered', [name, size, date_discovered], (error, results) => {
                    if (error) {
                        response.status(500).json({
                            success: false,
                            message: 'error',
                        });
                    }
                    response.status(201).json(results.rows);
                });
            }
        } catch (e) {
            return {
                error: e.message
            }
        }
    }

    patchPlanet(request, response) {
        const { id, name, size, date_discovered } = request.body;
        this.pool.query('UPDATE planet SET name= $1, size= $2, date_discovered= $3 WHERE id= $4', [name, size, date_discovered, id], (error, results) => {
            console.log(results);
            if (error) {
                response.status(500).json({
                    success: false,
                    message: 'error',
                });
            }
        });
        response.end();
    }

    deletePlanet(request, response) {
        const { id, name, size, date_discovered } = request.body;
        this.pool.query('DELETE FROM planet WHERE id=$1',[id], (error, results) => {
            if (error) {
                response.status(500).json({
                    success: false,
                    message: 'error',
                });
            }
        });
        response.end();
    }
}

module.exports.db = new Db();