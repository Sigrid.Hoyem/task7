const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const { db } = require('./db');

//Custom imports
const{Auth} = require('./Auth');
const publicPath = ['/api/v1/auth/generate-key'];

const auth = new Auth(db.getPool());

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use(async (request, response, next) =>{
   if(publicPath.includes(request.originalUrl)){
        return next();
    }

    const authResult = await auth.authenticateKey(request, response);

    if (authResult === true){
        next();
    } else{
        response.status(401).json(authResult);
    }
});

//Planets
app.get('/api/v1/planets', (request, response) => db.getPlanets(request, response));
app.post('/api/v1/planets/add', (request, response)=> db.addPlanet(request, response));
app.patch('/api/v1/planets/update', (request, response)=> db.patchPlanet(request, response));
app.delete('/api/v1/planets/delete', (request, response)=> db.deletePlanet(request, response));

//Authenticate
app.post('/api/v1/auth/generate-key', (request, response)=> auth.generateKey(request, response));


app.listen(port, ()=>{
    console.log('Server has connected to port 5000');
});