const crypto = require('crypto');

class Auth{

    constructor(pool){
        this.pool = pool;
    }

    async generateKey(request, response){
        try{
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query('INSERT INTO api_key (key) VALUES($1) RETURNING ID', [apiKey]);
            if(result.rowCount > 0){
                console.log('Successful: key generated');
            }
            response.json({
                apiKey
            });
        }
        catch(e){
            response.json({
                error: e.message
            });
        }
    }

    async authenticateKey(require, response){
        try{
            const{authorization} = require.headers;
            if(!authorization){
                return{
                    error: 'No API key found.'
                };
            }
            const key = authorization.split(' ')[1];
            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active =1', [key]);

            if (keyResult.rowCount > 0){
                return true;
            }else{
                return{
                    error: 'Invalid api key'
                };
            }
        }
        catch(e){
            return{
                error: e.message
            };
        }
    }
}

module.exports.Auth = Auth;